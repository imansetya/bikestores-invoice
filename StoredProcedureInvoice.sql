alter PROCEDURE usp_get_product(
				@order_id INT)
AS
BEGIN

	SELECT soit.order_id, pp.product_id, pp.product_name, soit.quantity, pp.list_price AS unit_price, SUM(soit.discount * pp.list_price * soit.quantity) AS discount, SUM((pp.list_price * soit.quantity)- (soit.discount * pp.list_price * soit.quantity)) AS amount
	FROM production.products pp
	JOIN sales.order_items soit ON pp.product_id = soit.product_id
	WHERE soit.order_id = @order_id
	GROUP BY soit.order_id,pp.product_id, pp.product_name, soit.quantity, pp.list_price
    
END

CREATE PROCEDURE usp_get_customer_details(@order_id INT)
AS
BEGIN
	SELECT sc.customer_id, sc.first_name, sc.last_name, sc.street, sc.city, sc.zip_code, sc.phone, so.order_id, so.order_date
		FROM sales.customers sc INNER JOIN sales.orders so
		ON sc.customer_id=so.customer_id WHERE order_id = @order_id
END

exec usp_get_customer_details 698;