/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package invoice.connection;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author imans
 */
public class InvoiceConnection {
    public final Connection conn;
    public final Statement st;

    public InvoiceConnection() throws SQLException {
        String connectionUrl = "jdbc:sqlserver://localhost:1433;" 
                +"databaseName=BikeStores;"
                +"user=sa;"
                +"password=12345;";
        conn = DriverManager.getConnection(connectionUrl);
        st=conn.createStatement();
    }

    public Connection getConn() {
        return conn;
    }

    public Statement getSt() {
        return st;
    }
}
