/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package invoice.service;

import invoice.dao.InvoiceDao;
import invoice.dao.InvoiceDaoImpl;
import invoice.model.Customer;
import invoice.model.OrderItem;
import java.sql.SQLException;
import static java.util.Collections.list;
import java.util.List;

/**
 *
 * @author imans
 */
public class InvoiceService {
    
    private InvoiceDao iao;

    public InvoiceService() throws SQLException {
        this.iao = new InvoiceDaoImpl();
       
    }
    
    public List<OrderItem> getAllOrderItem(int Order_Id) throws SQLException{
        if(iao.getAllOrderItem(Order_Id).isEmpty()){
            System.out.println("Result for order ID = "+Order_Id+" is Empty");
        }
        return iao.getAllOrderItem(Order_Id);
    }
    
    public Customer getDetailCustomer(int Order_Id) throws SQLException {
        if(iao.getAllOrderItem(Order_Id).isEmpty()){
           // System.out.println("Customer Data Not Found");
        }
        return iao.getDetailCustomer(Order_Id);
    }
    
    
}
