/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package invoice.dao;

import invoice.model.Customer;
import invoice.model.OrderItem;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author imans
 */
public interface InvoiceDao {
    public Customer getDetailCustomer(int OrderId) throws SQLException;
    public List<OrderItem> getAllOrderItem(int OrderId) throws SQLException;
}
