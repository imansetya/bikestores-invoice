/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package invoice.dao;

import invoice.connection.InvoiceConnection;
import invoice.model.Customer;
import invoice.model.OrderItem;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author imans
 */
public class InvoiceDaoImpl implements InvoiceDao{
        Connection conn;
        Statement stmt;
        private PreparedStatement getDetailCustomer;
        private PreparedStatement getAllOrderItem;
        
        private final String getDetailCustomerQuery="EXEC usp_get_customer_details @order_id = ?";
        private final String getAllOrderItemQuery="EXEC usp_get_product @order_id =?";
        
    public InvoiceDaoImpl() throws SQLException {
        InvoiceConnection icn = new InvoiceConnection();
        this.conn=icn.getConn();
        this.stmt=conn.createStatement();
        this.setKoneksi();
        
    }
    
    public final void setKoneksi() throws SQLException{
        getDetailCustomer=this.conn.prepareStatement(getDetailCustomerQuery);
        getAllOrderItem=this.conn.prepareStatement(getAllOrderItemQuery);
    }
    
    @Override
    public Customer getDetailCustomer(int OrderId) throws SQLException {
        //String SQL ="EXEC usp_get_customer_details ?"+OrderId;
        //ResultSet rs=this.stmt.executeQuery(SQL);
        getDetailCustomer.setInt(1, OrderId);
        ResultSet rs =this.getDetailCustomer.executeQuery();
        Customer csm = new Customer();
        while(rs.next()){
            csm.setFirstName(rs.getString("first_name"));
            csm.setLastName(rs.getString("last_name"));
            csm.setStreet(rs.getString("street"));
            csm.setCity(rs.getString("city"));
            csm.setZip(rs.getString("zip_code"));
            csm.setPhone(rs.getString("phone"));
            csm.setOrderId(rs.getInt("order_id"));
            csm.setOrderDate(rs.getString("order_date"));
        }
        return csm;
    }

    @Override
    public List<OrderItem> getAllOrderItem(int OrderId) throws SQLException {
        //String SQL ="EXEC usp_get_product @order_id ="+OrderId;
        //ResultSet rs=this.stmt.executeQuery(SQL);
        getAllOrderItem.setInt(1, OrderId);
        ResultSet rs =this.getAllOrderItem.executeQuery();
        List<OrderItem> loi = new ArrayList();
        while(rs.next()){
            OrderItem oit = new OrderItem();
            oit.setOrder_id(rs.getInt("order_id"));
            oit.setProduct_id(rs.getInt("product_id"));
            oit.setProduct_name(rs.getString("product_name"));
            oit.setQuantity(rs.getInt("quantity"));
            oit.setList_price(rs.getFloat("unit_price"));
            oit.setDiskon(rs.getFloat("discount"));
            oit.setAmount(rs.getFloat("amount"));
            loi.add(oit);
        }
        return loi;
        
    }
    
}
