/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package invoice.controller;

import invoice.view.InvoiceView;
import java.sql.SQLException;

/**
 *
 * @author imans
 */
public class InvoiceController {
    private final InvoiceView vw = new InvoiceView();
    public void ShowInvoice() throws SQLException{
        this.vw.showInvoice();
    }
}
