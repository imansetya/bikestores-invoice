/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package invoice.view;

import invoice.model.Customer;
import invoice.model.OrderItem;
import invoice.service.InvoiceService;
import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author imans
 */
public class InvoiceView {

    public void showInvoice() throws SQLException {
        Scanner scn = new Scanner(System.in);
        int Order_Id = 0;
        boolean kondisi = false;
        do {
            System.out.print("Masukkan Order ID : ");
            try {
                Order_Id = Integer.parseInt(scn.next());
            } catch (NumberFormatException e) {
                System.out.println("Masukan Angka untuk Order ID");
                kondisi = true;
                continue;
            }
            kondisi = false;
        } while (kondisi);

        InvoiceService is = new InvoiceService();
        Customer ctm = is.getDetailCustomer(Order_Id);
        List<OrderItem> oit = is.getAllOrderItem(Order_Id);
        if (!oit.isEmpty()) {
            System.out.println("#####################################################################################################################");
            System.out.println("#========================================================INVOICE====================================================#");
            System.out.println("#===================================================================================================================#");
            String fullname =ctm.getFirstName() + " " + ctm.getLastName();
            System.out.printf("%-116s%-1s\n","#"+fullname,"#");
            System.out.printf("%-116s%-1s\n","#"+ctm.getStreet(),"#");
            System.out.format("%-76s%-20s%-20s%-1s\n","#"+ctm.getCity(),"INVOICE#","DATE","#");
            System.out.format("%-76s%-20s%-20s%-1s\n","#"+ctm.getZip(),ctm.getOrderId(),ctm.getOrderDate(),"#");
            System.out.printf("%-116s%-1s\n","#"+ctm.getPhone(),"#");
            float total=0;
            System.out.println("#===================================================================================================================#");
            System.out.format("%-15s%-50s%-8s%-15s%-11s%-17s%1s\n", "#PRODUCT ID", "DESCRIPTION", "QTY","UNIT PRICE","DISCOUNT","AMOUNT","#");
            System.out.println("#-------------------------------------------------------------------------------------------------------------------#");
            for (OrderItem ot : oit) {
                System.out.format("%-1s%-15d%-50s%-8d%-15.2f%-11.2f%-15.2f%2s\n",
                        "#",
                        ot.getProduct_id(),
                        ot.getProduct_name(),
                        ot.getQuantity(),
                        ot.getList_price(),
                        ot.getDiskon(),
                        ot.getAmount(),
                        "#");
                total+=ot.getAmount();
               
            }
            System.out.println("#-------------------------------------------------------------------------------------------------------------------#");
            System.out.format("%-100s%-16.2f%1s\n","#TOTAL",total,"#");
            System.out.println("#####################################################################################################################");
        }

    }

}
